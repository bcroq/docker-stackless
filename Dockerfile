ARG BASE_IMAGE=debian:bullseye-slim

ARG DEBIAN_HTTP_PROXY=

# ----------------------------------------------------------------------------

FROM $BASE_IMAGE as build

# install needed libraries

RUN apt-get update \
 && apt-get install -y --no-install-recommends \
    ca-certificates \
    bzip2 \
    curl \
    file \
    gcc \
    libbz2-dev \
    libgdbm-dev \
    libc6-dev \
    libffi-dev \
    libreadline-dev \
    libsqlite3-dev \
    libssl-dev \
    make \
    zlib1g-dev

ARG STACKMAN_REF=dbc72fe5207a2055e658c819fdeab9731dee78b9
WORKDIR /usr/src/stackman
RUN curl -L https://github.com/stackless-dev/stackman/archive/${STACKMAN_REF}.tar.gz | tar -xzC /usr/src/stackman --strip-components=1

ARG SLP_VERSION=3.8.1
WORKDIR /usr/src/python
RUN curl -L https://github.com/stackless-dev/stackless/archive/v${SLP_VERSION}-slp.tar.gz | tar -xz --strip-components=1
RUN ./configure --prefix=/opt/stackless --with-lto --with-stackman=/usr/src/stackman
RUN make -j "$(nproc)" LDFLAGS="-Wl,--strip-all" \
 && make install
RUN /opt/stackless/bin/python3 -m ensurepip \
 && /opt/stackless/bin/pip3 --no-cache-dir install --upgrade pip setuptools virtualenv

# free some space

RUN find /opt/stackless -depth \
   \( \
      \( -type d -a \( -name test -o -name tests -o -name idle_test \) \) \
      -o \( -type f -a \( -name '*.exe' -o -name '*.pyo' -o -name '*.opt-1.pyc' -o -name '*.opt-2.pyc' -o -name '*.a' \) \) \
   \) -exec rm -rf '{}' +

# ----------------------------------------------------------------------------

FROM $BASE_IMAGE

RUN apt-get update \
 && apt-get install -y --no-install-recommends \
    ca-certificates \
    libbz2-1.0 \
    libffi7 \
    libgdbm6 \
    libreadline8 \
    libsqlite3-0 \
    libssl1.1 \
    xz-utils \
    zlib1g \
 && rm -rf /var/lib/apt/lists/*

# get stackless from the build stage

COPY --from=build /opt/stackless /opt/stackless
